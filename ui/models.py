import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Build(db.Model):
    build_id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    build_status = db.Column(db.String)
    build_device = db.Column(db.String)
    build_version = db.Column(db.String)
    build_type = db.Column(db.String)
    build_date = db.Column(db.Date)
    build_duration = db.Column(db.Integer)
    build_runner_id = db.Column(db.String, db.ForeignKey('runner.runner_id'))
    build_runner = db.relationship('Runner', backref=db.backref('builds', lazy=True))

    def __repr__(self):
        return f"{self.build_id} {self.build_device} {self.build_version} {self.build_type} {self.build_runner_id}"

    def as_dict(self):
        return {
            'id': self.build_id,
            'status': self.build_status,
            'device': self.build_device,
            'version': self.build_version,
            'type': self.build_type,
            'date': self.build_date.strftime('%Y-%m-%d'),
            'duration': self.build_duration,
            'runner': self.build_runner.runner_name if self.build_runner else None
        }

    @classmethod
    def get_or_create_by_id(cls, build_id):
        build = cls.query.filter_by(build_id=build_id).first()
        return build if build else cls(build_id=build_id)

    @classmethod
    def paginate(cls, args=None):
        if not args:
            args = {}
        return cls.query.filter_by(**args).order_by(cls.build_date.desc(), cls.build_id).paginate(per_page=min([args.get("per_page", 20), 200]))

class Runner(db.Model):
    runner_id = db.Column(db.String, primary_key=True, autoincrement=False)
    runner_name = db.Column(db.String)

    runner_sponsor = db.Column(db.String)
    runner_sponsor_url = db.Column(db.String)

    def as_dict(self):
        return {
            'id': self.runner_id,
            'name': self.runner_name,
            'sponsor': self.runner_sponsor,
            'sponsor_url': self.runner_sponsor_url
        }

    @classmethod
    def get_or_create_by_id(cls, runner_id):
        runner = cls.query.filter_by(runner_id=runner_id).first()
        if not runner:
            runner = cls(runner_id=runner_id)
            db.session.add(runner)
            db.session.commit()
        return runner

    @classmethod
    def get(cls, args=None):
        if not args:
            args = {}
        return cls.query.filter_by(**args).order_by(cls.runner_sponsor, cls.runner_name)
